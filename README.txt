This project is intended to be a drop-in replacement for forum module that
overrides default indexing behavior and queries to instead utilize the
entity_index project where appropriate, enhancing performance on very large
forums.

Last rolled against Drupal 7.38 version of forum module.


# Installation:

Required patches: See patches.make.


# General strategy

* Index as much as possible for topic listing / browsing displays.
* Drop lesser-used functionality in favor of optimizing performance, e.g.
  sorting topic listings. I'm willing to consider adding this back, but the
  effort required for this initial version was too high.

# Deprecated methods

* _forum_get_topic_order(): Method no longer needed.
