<?php

/**
 * @file
 * Drush-related callbacks for Fast Forum project.
 */

/**
 * Implementation of hook_drush_command().
 */
function forum_drush_command() {

  $items['forum-rebuild-ncs'] = array(
    'description' => 'Rebuild the node comment statistics table..',
    'examples' => array(
      'drush forum-rebuild-ncs',
    ),
    'callback' => 'drush_forum_rebuild_ncs',
  );

  return $items;
}

/**
 * Drush callback.
 *
 * This method is a direct copy of devel_rebuild_node_comment_statistics(), but
 * is included here so that node comment statistics can be rebuilt without devel
 * module enabled.
 *
 * @see devel_rebuild_node_comment_statistics()
 */
function drush_forum_rebuild_ncs() {
  // Empty table.
  db_truncate('node_comment_statistics')->execute();

  $sql = "
    INSERT IGNORE INTO {node_comment_statistics} (nid, cid, last_comment_timestamp, last_comment_name, last_comment_uid, comment_count) (
      SELECT c.nid, c.cid, c.created, c.name, c.uid, c2.comment_count FROM {comment} c
      JOIN (
        SELECT c.nid, MAX(c.created) AS created, COUNT(*) AS comment_count FROM {comment} c WHERE status = 1 GROUP BY c.nid
      ) AS c2 ON c.nid = c2.nid AND c.created = c2.created
    )";
  db_query($sql, array(':published' => COMMENT_PUBLISHED));

  // Insert records into the node_comment_statistics for nodes that are missing.
  $query = db_select('node', 'n');
  $query->leftJoin('node_comment_statistics', 'ncs', 'ncs.nid = n.nid');
  $query->addField('n', 'changed', 'last_comment_timestamp');
  $query->addField('n', 'uid', 'last_comment_uid');
  $query->addField('n', 'nid');
  $query->addExpression('0', 'comment_count');
  $query->addExpression('NULL', 'last_comment_name');
  $query->isNull('ncs.comment_count');

  db_insert('node_comment_statistics', array('return' => Database::RETURN_NULL))
    ->from($query)
    ->execute();
}
