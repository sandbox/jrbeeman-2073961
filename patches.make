api = 2
core = 7.x

# https://www.drupal.org/node/1158114
# SelectQuery::fields() must be an array, null given entity.inc on line 284 SelectQuery->fields()...
projects[drupal][patch][] = "https://www.drupal.org/files/issues/drupal-component-1158114-63.patch"
