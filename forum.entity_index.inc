<?php

/**
 * @file
 * Entity index schema and index alterations for forum module.
 */

/**
 * Implements hook_entity_index_fields().
 *
 * last_comment_timestamp: Already covered in entity_index_node.
 */
function forum_entity_index_fields($entity_type) {
  $fields = array();

  // Emulate the forum_index.
  if ($entity_type == 'node') {
    $fields = array(
      'node' => array(
        'title' => 'title',
        'comment' => 'comment_mode',
      ),
      'node_comment_statistics' => array(
        'comment_count' => 'comment_count',
        'cid' => 'last_comment_cid',
        'last_comment_name' => 'last_comment_name',
        'last_comment_uid' => 'last_comment_uid',
      ),
      'forum' => array(
        'tid' => 'forum_tid',
      ),
      'users' => array(
        'name' => 'author_name',
      ),
    );
  }

  return $fields;
}

/**
 * Implements hook_entity_index_indexes().
 *
 * created: Already covered in entity_index_node.
 * last_comment_timestamp: Already covered in entity_index_node.
 */
function forum_entity_index_indexes($entity_type) {
  $indexes = array();

  if ($entity_type == 'node') {
    $indexes['forum_last_post'] = array('forum_tid', 'last_comment_timestamp');
    $indexes['forum_topics'] = array('forum_tid', 'sticky', 'last_comment_timestamp');
    $indexes['comment_count'] = array('comment_count');
    $indexes['topics_comment_count'] = array('forum_tid', 'comment_count');
  }

  return $indexes;
}

/**
 * Implements hook_entity_index_populate_data().
 */
function forum_entity_index_populate_data($entity_type, &$query) {
  if ($entity_type == 'node') {
    $query->fields('node', array('title'));
    $query->addField('node', 'comment', 'comment_mode');

    // A join on node_comment_statistics has already happened, aliased as 'ncs'
    // in entity_index_node, so just add fields without joining.
    $query->addField('ncs', 'cid', 'last_comment_cid');
    $query->fields('ncs', array('comment_count', 'last_comment_uid'));

    $query->leftJoin('forum', 'f', 'f.nid = node.nid');
    $query->addField('f', 'tid', 'forum_tid');

    $query->leftJoin('users', 'u', 'u.uid = node.uid');
    $query->addField('u', 'name', 'author_name');

    // Deal with last_comment_name being null when users are authenticated.
    $query->join('users', 'u2', 'ncs.last_comment_uid = u2.uid');
    $query->addExpression('CASE ncs.last_comment_uid WHEN 0 THEN ncs.last_comment_name ELSE u2.name END', 'last_comment_name');
  }
}
